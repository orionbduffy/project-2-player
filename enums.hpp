//
// Created by jacks on 2/14/2021.
//

#pragma once
#include "tools.hpp"

enum ColorEnum {white, orange, yellow, green, blue};

extern const char* words[];

inline ostream& operator<< (ostream& os, ColorEnum color)
{
    return os << " Color: " << words[color] << "\t";
}