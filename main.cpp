#include "tools.hpp"
#include "player.hpp"
#include "enums.hpp"

string chooseName();
ColorEnum chooseColor();
Player* createPlayer();

int main ()
{
    Player* player;

    cout << "Test: Create a very long name. Numbers and special characters are allowed \n" <<
    "Try 10, -10, w, hello, and then b for the color." << endl;
    player = createPlayer();
    cout << player;

    cout << player->wonColumn(27) << "\t";
    cout << player;

    cout << player->wonColumn(2) << "\t";
    cout << player;

    cout << player->wonColumn(2) << "\t";
    cout << player;

    cout << player->wonColumn(5) << "\t";
    cout << player;

    cout << player->wonColumn(7) << "\t";
    cout << player;

    cout << player->wonColumn(8) << "\t";
    cout << player;

    return 0;
}

Player* createPlayer()
{
    return new Player(chooseName(), chooseColor());
}

string chooseName()
{
    string name;

    cout << "Please choose a name" << endl;
    cin >> name >> flush;

    return name;
}

ColorEnum chooseColor()
{
    char color;
    bool invalidInput = true;
    string validInputs = "oygb";

    while (invalidInput)
    {
        cout << "Please choose a color by typing it or its first letter in\n" <<
             "The options are: orange, yellow, green, blue" << endl;
        cin >> color >> flush;

        if (validInputs.find(color) == string::npos)
            cout << "Error! Invalid input! Input was not orange, yellow, green, or blue" << endl;
        else
            invalidInput = false;
    }

    switch (color)
    {
        case 'o':
            return orange;
        case 'y':
            return yellow;
        case 'g':
            return green;
        case 'b':
            return blue;
        default:
            throw runtime_error("The input for the player's color made it past the error checks despite being invalid");
    }
}