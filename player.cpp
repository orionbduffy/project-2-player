//
// Created by jacks on 2/14/2021.
//

#include "player.hpp"

bool Player::wonColumn (int columnNum)
{
    columns[score++] = columnNum;

    return score >= 3;
}

ostream& Player::print(ostream& os)
{
    return os << "Name: " << name << "\t" << color << " Score: " << score << "\t" <<
    " Columns Won: " << columns[0] << ", " << columns[1] << ", " << columns[2] << endl;
}