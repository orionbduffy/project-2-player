//
// Created by jacks on 2/14/2021.
//

#pragma once
#include "tools.hpp"
#include "enums.hpp"

class Player
{
private:
    string name;
    ColorEnum color;
    int score = 0;
    int columns[3]{0};

public:
    Player(string name, ColorEnum color): name(name), color(color){}

    ~Player() = default;

    ColorEnum getColor() { return color; }
    int getScore() { return score; }

    bool wonColumn (int columnNum);

    ostream& print(ostream&);
};

inline ostream& operator<< (ostream& os, Player* player)
{
    return player->print(os);
}